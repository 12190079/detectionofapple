# Automated Detection of Bad Apples Using Image Classification

A web application developed for agricultural industry to identify the bad apples from good apples. 
This system helps:
To provide timely and accurate detection of good and bad apples.
To enhance the quality, quantity and stability of apples through forecasting.
To reduce human workload for identifying the bad quality apple manually.


Built using: Python, TensorFlow, HTML/CSS/JS, Bootstrap, Django, PostgreSQL

Try the web application on: https://apple-detection.herokuapp.com/ 

Watch the promotional video: 

## Web Application

![1](/uploads/68a5d3dabdd4c5a74a9028584697820c/1.png)
![2](/uploads/d92ec29613fbdc8694a9ddb3013670a0/2.png)
![3](/uploads/c3fbe1301e73a5e93837dc13ed246653/3.png)

## Poster


