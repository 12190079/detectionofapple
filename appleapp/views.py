from django.shortcuts import render, redirect
# from django.http import HttpResponse
from django.core.files.storage import FileSystemStorage
import tensorflow as tf
from keras.models import load_model
# from keras.preprocessing import image
import json
from tensorflow import Graph
from .models import history
import base64

# Create your views here.
img_height, img_width = 224,224
with open ('./model/classes.json','r') as f:
    labelInfo = f.read()
labelInfo = json.loads(labelInfo)

model_graph = Graph()
with model_graph.as_default():
    tf_session = tf.compat.v1.Session()
    with tf_session.as_default():
        model = load_model('./model/apple_model.h5')

def predictImage(request):
    print(request)
    # print(request.POST.dict())
    # print(request.FILES['filePath'])
    # fileObj = request.FILES['filePath']
    # fs = FileSystemStorage()
    # filePathName = fs.save(fileObj.name,fileObj)
    # filePathName = fs.url(filePathName)
    # testimage ='.'+filePathName
    filePathName  = request.POST.get('filePath','')
    split = filePathName.split(",")[-1]
    b = base64.b64decode(split)
    fh = open("image.jpg","wb")
    fh.write(b)
    fh.close()

    # fs = FileSystemStorage()

    # uploaded_file = request.FILES['filePath']
    # b = uploaded_file.read(1000000000)
    # fh = open("image.jpg","wb")
    # print(fs.url("image.jpg"))
    # fh.write(b)
    # fh.close()
    img = tf.keras.utils.load_img('image.jpg', target_size = (img_height,img_width))
    x = tf.keras.utils.img_to_array(img)
    x=x/255
    x = x.reshape(1,img_height,img_width,3)
    with model_graph.as_default():
        with tf_session.as_default():
            predi = model.predict(x)
        
    import numpy as np
    predictedLabel = labelInfo[str(np.argmax(predi[0]))]

    # imageurl = fs.url("image.jpg")
    # imagename = imageurl.split("/")[-1]
    # print(imagename)


    # context = {'imageurl': imageurl,'filePathName':filePathName, 'predictedLabel':predictedLabel[0]}
    context = {'filePathName':filePathName, 'predictedLabel':predictedLabel[0]}
    return render(request, 'result.html', context )


def uploadpredictImage(request):
    # print(request)
    filePathName  = request.POST.get('filePath','')
    uploaded_file = request.FILES['image']
    b = uploaded_file.read(1000000000)
    fh = open("image.jpg","wb")
    # print(fs.url("image.jpg"))
    fh.write(b)
    fh.close()
    img = tf.keras.utils.load_img('image.jpg', target_size = (img_height,img_width))
    x = tf.keras.utils.img_to_array(img)
    x=x/255
    x = x.reshape(1,img_height,img_width,3)
    with model_graph.as_default():
        with tf_session.as_default():
            predi = model.predict(x)
        
    import numpy as np
    predictedLabel = labelInfo[str(np.argmax(predi[0]))]

    context = {'filePathName':filePathName, 'predictedLabel':predictedLabel[0]}
    return render(request, 'result.html', context )


def index(request):
    return render(request,'index.html')
def about(request):
    return render(request,"about.html")
def camera(request):
    return render(request,'camera.html')
def viewhistory(request):
    items = history.objects.all()
    return render(request,'viewhistory.html',{'items':items})
def createItem(request):
    item = history()
    item.results = request.POST.get('text')
    item.save()
    return redirect('/')
    # return render(request, 'viewhistory.html')    
def clear_history(request):
    clear = history.objects.all()
    clear.delete()
    return render(request, 'viewhistory.html')
